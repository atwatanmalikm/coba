```
Environment :
OS : Ubuntu 18.04
CPU : 8
RAM : 23 GB
Disk : 100 GB

Openstack Version : Ussuri
```

**=== ENABLE SENLIN ===**

1. Enable service senlin

2. Reconfigure
> kolla-ansible -i ./all-in-one reconfigure


# MEMBUAT CLUSTER 

1. Membuat file profile
> vim profilecirros.yml

```
type: os.nova.server
version: 1.0
properties:
  name: cirros_server
  flavor: minimum
  image: "cirros"
  key_name: key
  networks:
   - network: internal
  metadata:
    test_key: test_value
  user_data: |
    #!/bin/sh
    echo 'hello, world' > /tmp/test_file
```

2. Membuat profile
> openstack cluster profile create --spec-file profilecirros.yml cirros_profile

> openstack cluster profile list


3. Membuat cluster
> openstack cluster create --profile cirros_profile --desired-capacity 1 mycluster
> openstack cluster show mycluster


4. Cek node
> openstack server list



``

# SCALING CLUSTER

***## scale up ##***

1. Scale up 1 node
> openstack cluster expand mycluster

2. Cek detail cluster
> openstack cluster show mycluster


*Pastikan desired_capacity pada status_reason bertambah

3. Scale up menjadi beberapa node
> openstack cluster expand --count 2 mycluster

4. Cek detail cluster
openstack cluster show mycluster


***## scale down ##***

1. Scale down 1 node
> openstack cluster shrink mycluster

2. Cek detail cluster
openstack cluster show mycluster


3. Scale down menjadi beberapa node 
> openstack cluster shrink --count 2 mycluster

4. Cek detail cluster
> openstack cluster show mycluster



*Pastikan desired_capacity pada status_reason berkurang


***## Resize jumlah node cluster ##***


1. Resize dengan jumlah node tertentu
> openstack cluster resize --capacity 3 mycluster

2. Cek detail cluster
> openstack cluster show mycluster


*Pastikan desired_capacity pada status_reason berkurang


# MEMBUAT NODE & MENAMBAH NODE KE CLUSTER 


1. Membuat node
> openstack cluster node create --profile cirros_profile newnode

2. Cek list node
> openstack cluster node list

3. Menambahkan node ke cluster
> openstack cluster members add --nodes newnode mycluster

4. Cek list node mycluster
> openstack cluster members list mycluster


# Auto Scaling Cluster menggunakan Webhook Receiver

***## scale out ##***
> openstack cluster members list cluster_cirros_basic
```
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| 7b7046f3 | node-UyvLrXdM |     4 | ACTIVE | c822834e    | 2020-08-06T14:54:03Z |
| 6e4af96a | node-aXZc76KC |     6 | ACTIVE | 07fb2d71    | 2020-08-06T15:17:05Z |
| 784d9757 | node-IxXcLYoW |     7 | ACTIVE | 4db9c104    | 2020-08-06T15:17:05Z |
| 9a527f1d | node-u9yWF9AZ |     5 | ACTIVE | dc4ffac2    | 2020-08-06T15:17:03Z |
| 2f500d88 | node-OdIfekmG |     8 | ACTIVE | 3fa1db1b    | 2020-08-06T19:06:01Z |
+----------+---------------+-------+--------+-------------+----------------------+
```

* Membuat Receiver dengan tipe webhook
> openstack cluster receiver create --type webhook --cluster cluster_cirros_basic --action CLUSTER_SCALE_OUT basic_receiver_elastic_scaleout
 
> *"alarm_url": "http://10.5.5.50:8778/v1/webhooks/1fc6852e-9f67-4121-93e0-d13548f823aa/trigger?V=2"

* Buat Policy untuk menambah 2 node
vim scaling_policy_basic.yml
```
...
type: senlin.policy.scaling
version: 1.0
properties:
  event: CLUSTER_SCALE_OUT
  adjustment:
    type: CHANGE_IN_PERCENTAGE
    number: 1
    min_step: 2
    best_effort: True
    cooldown: 120
...
```
* Create policy dan attach ke cluster
> openstack cluster policy create --spec-file scaling_policy_basic.yml scaling_policy_basic

> openstack cluster policy attach --policy scaling_policy_basic cluster_cirros_basic

> openstack cluster policy binding list cluster_cirros_basic

* Nodes yang terdapat pada cluster sebelum scaling
ubuntu@riset-btech:~$ openstack cluster members list cluster_cirros_basic
```
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| 9a527f1d | node-u9yWF9AZ |     5 | ACTIVE | dc4ffac2    | 2020-08-06T15:17:03Z |
| 2f500d88 | node-OdIfekmG |     8 | ACTIVE | 3fa1db1b    | 2020-08-06T19:06:01Z |
| 630d0792 | node-pX21MGGm |     9 | ACTIVE | f473918a    | 2020-08-06T19:17:27Z |
| ee582861 | node-MzExlRwY |    10 | ACTIVE | 6d548c9d    | 2020-08-06T19:47:08Z |
+----------+---------------+-------+--------+-------------+----------------------+
```
* akses webhook receiver
> curl -X POST http://10.5.5.50:8778/v1/webhooks/1fc6852e-9f67-4121-93e0-d13548f823aa/trigger?V=2  
* Kemudian cek nodes dalam cluster  
> ubuntu@riset-btech:~$ openstack cluster members list cluster_cirros_basic
```
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| 9a527f1d | node-u9yWF9AZ |     5 | ACTIVE | dc4ffac2    | 2020-08-06T15:17:03Z |
| 2f500d88 | node-OdIfekmG |     8 | ACTIVE | 3fa1db1b    | 2020-08-06T19:06:01Z |
| 630d0792 | node-pX21MGGm |     9 | ACTIVE | f473918a    | 2020-08-06T19:17:27Z |
| ee582861 | node-MzExlRwY |    10 | ACTIVE | 6d548c9d    | 2020-08-06T19:47:08Z |
| a5aa6249 | node-CBRJX9ri |    11 | ACTIVE | 3eb9705e    | 2020-08-06T20:02:59Z |
| ecad9ef2 | node-JYAspMlR |    12 | ACTIVE | 6b948b08    | 2020-08-06T20:03:01Z |
+----------+---------------+-------+--------+-------------+----------------------+
```

# Service yang bisa dibuat cluster oleh Senlin dalam cluster openstack ini
> ubuntu@riset-btech:~$ openstack cluster profile type list
```
+----------------------------+---------+----------------------------+
| name                       | version | support_status             |
+----------------------------+---------+----------------------------+
| container.dockerinc.docker | 1.0     | EXPERIMENTAL since 2017.02 |
| os.heat.stack              | 1.0     | SUPPORTED since 2016.04    |
| os.nova.server             | 1.0     | SUPPORTED since 2016.04    |
+----------------------------+---------+----------------------------+
```


***## scale in ##***

>openstack cluster receiver create --type webhook --cluster cluster_cirros_basic --action CLUSTER_SCALE_IN basic_receiver_elastic_scalein  
"alarm_url": "http://10.5.5.50:8778/v1/webhooks/5ea73622-d156-4cf7-9ab4-27e71d7da79f/trigger?V=2"

* Create policy spec file
```
vim scalingin_policy_basic.yml
...
type: senlin.policy.scaling
version: 1.0
properties:
  event: CLUSTER_SCALE_IN
  adjustment:
    type: CHANGE_IN_PERCENTAGE
    number: 1
    min_step: 3
    best_effort: True
    cooldown: 120
...
```
> openstack cluster policy create --spec-file scalingin_policy_basic.yml scalingin_policy_basic

> openstack cluster policy attach --policy scalingin_policy_basic cluster_cirros_basic  
openstack cluster policy binding list cluster_cirros_basic

ubuntu@riset-btech:~$ openstack cluster members list cluster_cirros_basic  
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| 9a527f1d | node-u9yWF9AZ |     5 | ACTIVE | dc4ffac2    | 2020-08-06T15:17:03Z |
| 2f500d88 | node-OdIfekmG |     8 | ACTIVE | 3fa1db1b    | 2020-08-06T19:06:01Z |
| 630d0792 | node-pX21MGGm |     9 | ACTIVE | f473918a    | 2020-08-06T19:17:27Z |
| a5aa6249 | node-CBRJX9ri |    11 | ACTIVE | 3eb9705e    | 2020-08-06T20:02:59Z |
| ecad9ef2 | node-JYAspMlR |    12 | ACTIVE | 6b948b08    | 2020-08-06T20:03:01Z |
+----------+---------------+-------+--------+-------------+----------------------+

* kirim request scale in untuk mengecilkan cluster size
>curl -X POST http://10.5.5.50:8778/v1/webhooks/5ea73622-d156-4cf7-9ab4-27e71d7da79f/trigger?V=2

ubuntu@riset-btech:~$ openstack cluster members list cluster_cirros_basic   
```
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| a5aa6249 | node-CBRJX9ri |    11 | ACTIVE | 3eb9705e    | 2020-08-06T20:02:59Z |
| ecad9ef2 | node-JYAspMlR |    12 | ACTIVE | 6b948b08    | 2020-08-06T20:03:01Z |
+----------+---------------+-------+--------+-------------+----------------------+
```


# AUTO RECOVERING NODE FAILURES

1. Membuat health policy
> vim health.yml

```
# Sample health policy based on node health checking
type: senlin.policy.health
version: 1.1
description: A policy for maintaining node health from a cluster.
properties:
  detection:
    # Number of seconds between two adjacent checking
    interval: 600

    detection_modes:
      # Type for health checking, valid values include:
      # NODE_STATUS_POLLING, NODE_STATUS_POLL_URL, LIFECYCLE_EVENTS
      - type: NODE_STATUS_POLLING

  recovery:
    # Action that can be retried on a failed node, will improve to
    # support multiple actions in the future. Valid values include:
    # REBOOT, REBUILD, RECREATE
    actions:
      - name: RECREATE
```

2. Membuat policy
> openstack cluster policy create --spec-file health.yml health_policy

3. Attach policy ke cluster
> openstack cluster policy attach --policy scaling_policy mycluster

4. Cek list policy yang di attach ke mycluster
> openstack cluster policy binding list mycluster

5. Cek list node di mycluster
> openstack cluster members list mycluster

6. Matikan salah satu server yang memiliki ID sama dgn node di cluster :
> openstack server stop 90e3c43f-abf9-4c9d-a57b-a71c7c3e6d73

7. Cek list node di mycluster
> watch openstack cluster members list mycluster


# Membuat Stack Senlin menggunakan Heat

* Membuat stack
> git clone https://opendev.org/openstack/heat-templates  
cd heat-templates/hot/senlin  
openstack stack create -t cluster.yaml --parameter key_name=mykey --parameter image=ubuntu-18.04 --parameter flavor=m1.small --parameter network=demo-net senlin_stack  

* Check stack yang telah dibuat oleh Heat

> ubuntu@riset-btech:~/heat-templates/hot/senlin$ openstack stack list
```
+--------------------------------------+--------------+----------------------------------+-----------------+----------------------+--------------+
| ID                                   | Stack Name   | Project                          | Stack Status    | Creation Time        | Updated Time |
+--------------------------------------+--------------+----------------------------------+-----------------+----------------------+--------------+
| 6df19e5d-1ad9-46c0-ab16-4a9219e4e842 | senlin_stack | 0cf4430577c74de79dfb601f89abb431 | CREATE_COMPLETE | 2020-08-07T12:36:40Z | None         |
+--------------------------------------+--------------+----------------------------------+-----------------+----------------------+--------------+
```

* check receiver Senlin yang dibuat oleh Heat
>ubuntu@riset-btech:~/heat-templates/hot/senlin$ openstack cluster receiver show senlin_stack-receiver_scale_in-zge5yvoymd4c | grep alarm_url  
|            |   "alarm_url": "http://10.5.5.50:8778/v1/webhooks/5f5ad752-f38e-4f42-bfbb-1d1c69657cd5/trigger?V=2"  
ubuntu@riset-btech:~/heat-templates/hot/senlin$ openstack cluster receiver show senlin_stack-receiver_scale_out-65rfndjsyxwj | grep alarm_url  
|            |   "alarm_url": "http://10.5.5.50:8778/v1/webhooks/ec109774-f023-456d-b186-c3e6e7a6c3bc/trigger?V=2"  

* check member list cluster Senlin yang dibuat oleh Heat
> ubuntu@riset-btech:~/heat-templates/hot/senlin$ openstack cluster members list senlin_stack-cluster-wzhwra7lgzou  
```
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| a30ba11b | node-W8NG2jqp |     1 | ACTIVE | 05d32e81    | 2020-08-07T12:36:58Z |
+----------+---------------+-------+--------+-------------+----------------------+
```

* Terjadi `error` ketika mengirim request scale out via webhook receiver senlin
```
curl -X POST http://10.5.5.50:8778/v1/webhooks/ec109774-f023-456d-b186-c3e6e7a6c3bc/trigger?V=2
+----------+---------------+-------+----------+-------------+----------------------+
| id       | name          | index | status   | physical_id | created_at           |
+----------+---------------+-------+----------+-------------+----------------------+
| a30ba11b | node-W8NG2jqp |     1 | ACTIVE   | 05d32e81    | 2020-08-07T12:36:58Z |
| 4699850c | node-nWyohByc |     2 | ERROR    | None        | None                 |
+----------+---------------+-------+----------+-------------+----------------------+
```

masalah error ketika mengirim request scale out via webhook receiver senlin berhasil ketika diujicoba menambahkan `max_size=5 cluster` menggunakan image cirros.

* template cluster_cirros.yaml
```
heat_template_version: 2016-04-08

description: >
  This template demostrate how to use senlin resources to create
  a flexible cluster.

parameters:
  flavor:
    description: Flavor for the instances to be created.
    type: string
    default: m1.tiny
  image:
    description: Name or ID of the image to use for the instances.
    type: string
    default: cirros
  key_name:
    description: Name of an existing key pair to use for the instances.
    type: string
  network:
    description: The network for the instances.
    type: string
    default: demo-net

resources:
  profile:
    type: OS::Senlin::Profile
    properties:
      type: os.nova.server-1.0
      properties:
        flavor: {get_param: flavor}
        image: {get_param: image}
        key_name: {get_param: key_name}
        networks:
          - network: {get_param: network}

  cluster:
    type: OS::Senlin::Cluster
    properties:
      desired_capacity: 2
      max_size: 5
      profile: {get_resource: profile}

  scale_in_policy:
    type: OS::Senlin::Policy
    properties:
      type: senlin.policy.scaling-1.0
      bindings:
        - cluster: {get_resource: cluster}
      properties:
        event: CLUSTER_SCALE_IN
        adjustment:
          type: CHANGE_IN_CAPACITY
          number: 1

  scale_out_policy:
    type: OS::Senlin::Policy
    properties:
      type: senlin.policy.scaling-1.0
      bindings:
        - cluster: {get_resource: cluster}
      properties:
        event: CLUSTER_SCALE_OUT
        adjustment:
          type: CHANGE_IN_CAPACITY
          number: 1

  receiver_scale_out:
    type: OS::Senlin::Receiver
    properties:
      cluster: {get_resource: cluster}
      action: CLUSTER_SCALE_OUT
      type: webhook

  receiver_scale_in:
    type: OS::Senlin::Receiver
    properties:
      cluster: {get_resource: cluster}
      action: CLUSTER_SCALE_IN
      type: webhook

outputs:
  webhook_scale_out:
    description: Webhook to scale out cluster.
    value:
      str_replace:
        template: curl -X POST LINK
        params:
          LINK: {get_attr: [receiver_scale_out, channel, alarm_url]}

  webhook_scale_in:
    description: Webhook to scale in cluster.
    value:
      str_replace:
        template: curl -X POST LINK
        params:
          LINK: {get_attr: [receiver_scale_in, channel, alarm_url]}
```

* test scale out
```
ubuntu@riset-btech:~$ openstack cluster members list senlin_cirros_max-cluster-z5ixmmyd3kfb
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| 1a85ff55 | node-UGXvUd7F |     1 | ACTIVE | 45635c51    | 2020-08-10T06:39:31Z |
| adae54e9 | node-LISqR0Ui |     3 | ACTIVE | 9f8457cb    | 2020-08-10T06:41:00Z |
+----------+---------------+-------+--------+-------------+----------------------+
```
> ubuntu@riset-btech:~$ curl -X POST http://10.5.5.50:8778/v1/webhooks/bb2c7bab-90d8-48c6-93cb-01c9e6243ee3/trigger?V=2

```
+----------+---------------+-------+----------+-------------+----------------------+
| id       | name          | index | status   | physical_id | created_at           |
+----------+---------------+-------+----------+-------------+----------------------+
| 1a85ff55 | node-UGXvUd7F |     1 | ACTIVE   | 45635c51    | 2020-08-10T06:39:31Z |
| adae54e9 | node-LISqR0Ui |     3 | ACTIVE   | 9f8457cb    | 2020-08-10T06:41:00Z |
| 26cb7592 | node-XnoLat9p |     4 | CREATING | None        | None                 |
+----------+---------------+-------+----------+-------------+----------------------+
```


## Membuat Cluster Heat Stack 

1. Membuat stack template agar stack membuat 2 instance
> vim heat_template.yaml

```
heat_template_version: 2018-08-31

description: Simple template to deploy a single compute instance

resources:
  instance1:
    type: OS::Nova::Server
    properties:
      flavor: m1.tiny
      image: cirros
      networks:
        - network: internal

  instance2:
    type: OS::Nova::Server
    properties:
      flavor: m1.tiny
      image: cirros
      networks:
        - network: internal
```

2. Membuat profile untuk stack
> vim cluster_stack.yaml
```
type: os.heat.stack
version: 1.0
properties:
  name: mystack
  template: heat_template.yaml
  context:
    region_name: RegionOne
```
> openstack cluster profile create --spec-file heat_template.yaml heat_profile

3. Membuat cluster 
> openstack cluster create --profile heat_profile mycluster_stack

4. Expand cluster 
> openstack cluster expand mycluster_stack

5. Cek list member cluster
> openstack cluster members list mycluster_stack

6. Cek list instance yang dibuat stack
> openstack stack resource list node-rFLiP2Dj-aXrSq9Y3


# Senlin Manual Deployment

## Clone Repo dan install package requirements
```
mkdir /opt/stack
cd /opt/stack
git clone -b stable/ussuri https://git.openstack.org/openstack/senlin.git
pip3 install -e .
```
## Buat Endpoint dan Assign Role
```
openstack user create --domain default --password-prompt clustering
pass : qwerty
openstack role add --project service --user clustering admin
openstack service create --name neutron --description "OpenStack Networking" clustering
openstack endpoint create --region RegionOne clustering public http://controller:8770
openstack endpoint create --region RegionOne clustering internal http://controller:8770
openstack endpoint create --region RegionOne clustering admin http://controller:8770
```
# Generate file konfig
```
root@controller:/opt/stack/senlin# tools/gen-config
root@controller:/opt/stack/senlin# mkdir /etc/senlin
root@controller:/opt/stack/senlin# cp etc/senlin/api-paste.ini /etc/senlin/
root@controller:/opt/stack/senlin# cp etc/senlin/senlin.conf.sample /etc/senlin/senlin.conf
root@controller:/opt/stack/senlin# vi /etc/senlin/senlin.conf
```
## Config senlin
```
vi /etc/senlin/senlin.conf

[database]
connection = mysql+pymysql://senlin:senlin_qwerty@controller/senlin?charset=utf8

[keystone_authtoken]
service_token_roles_required = True
auth_type = password
user_domain_name = Default
project_domain_name = Default
project_name = service
username = senlin
password = qwerty
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211

[authentication]
auth_url = http://controller:5000
service_username = senlin
service_password = qwerty
service_project_name = service

[oslo_messaging_rabbit]
rabbit_userid = openstack
rabbit_hosts = controller:5672
rabbit_password = qwerty

[oslo_messaging_notifications]
driver = messaging
```
## CREATE DATABASE
```
cd /opt/stack/senlin/tools
./senlin-db-recreate

mysql -u root -pqwerty
DROP DATABASE IF EXISTS senlin;
CREATE DATABASE senlin DEFAULT CHARACTER SET utf8;
GRANT ALL ON senlin.* TO 'senlin'@'localhost' IDENTIFIED BY 'senlin_qwerty';
GRANT ALL ON senlin.* TO 'senlin'@'%' IDENTIFIED BY 'senlin_qwerty';
flush privileges;

senlin-manage db_sync
```

## Buat Service Daemon
```
root@controller:/opt/stack/senlin/tools# cat /lib/systemd/system/senlin-conductor.service
[Unit]
Description=Example systemd service.                                                           
[Service]
Type=simple
ExecStart=senlin-conductor --config-file /etc/senlin/senlin.conf

[Install]
WantedBy=multi-user.target

vim /lib/systemd/system/senlin-engine.service
[Unit]
Description=Example systemd service.                                                           
[Service]
Type=simple
ExecStart=senlin-engine --config-file /etc/senlin/senlin.conf

[Install]
WantedBy=multi-user.target

vim /lib/systemd/system/senlin-health-manager.service
[Unit]
Description=Example systemd service.                                                           

[Service]
Type=simple
ExecStart=senlin-health-manager --config-file /etc/senlin/senlin.conf

[Install]
WantedBy=multi-user.target

vim /lib/systemd/system/senlin-api.service
[Unit]
Description=Example systemd service.                                                           

[Service]
Type=simple
ExecStart=senlin-api --config-file /etc/senlin/senlin.conf

[Install]
WantedBy=multi-user.target
```
## Update Daemon
```
systemctl daemon-reload

systemctl start senlin-conductor.service
systemctl start senlin-engine.service
systemctl start senlin-health-manager.service
systemctl start senlin-api.service

systemctl status senlin-conductor.service
systemctl status senlin-engine.service
systemctl status senlin-health-manager.service
systemctl status senlin-api.service
```
## Edit Senlin port, karena port default 8778 digunakan oleh placement
```
vi /etc/senlin/senlin.conf
[senlin_api]
...
bind_host = 192.168.10.151
bind_port = 8770
```
## SENLIN CLIENT
```
git clone -b stable/ussuri https://git.openstack.org/openstack/python-senlinclient.git
cd python-senlinclient
sudo python setup.py install
# Verify
openstack cluster build info
```
## Test create cluster
```
root@controller:~# openstack cluster list
+----------+----------------+--------+----------------------+----------------------+
| id       | name           | status | created_at           | updated_at           |
+----------+----------------+--------+----------------------+----------------------+
| dba7299a | cluster_dicoba | ACTIVE | 2020-08-18T03:06:33Z | 2020-08-18T03:06:33Z |
+----------+----------------+--------+----------------------+----------------------+
root@controller:~# openstack cluster member list dba7299a
+----------+---------------+-------+--------+-------------+----------------------+
| id       | name          | index | status | physical_id | created_at           |
+----------+---------------+-------+--------+-------------+----------------------+
| 1ebf9fa8 | node-ZPbkmr9c |     1 | ACTIVE | 023fcac6    | 2020-08-18T03:06:31Z |
+----------+---------------+-------+--------+-------------+----------------------+
```
